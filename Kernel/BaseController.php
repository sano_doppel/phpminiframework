<?php
namespace Kernel;

use Kernel\Observer\Observable;
use Kernel\View\ViewContext;

/**
 * Class BaseController
 * Created by Alexander Petrov.
 */
abstract class BaseController
{

    /**
     * @var \Kernel\Config\Config
     */
    protected $config;

    /**
     * @var \Kernel\DB\DBContext
     */
    protected $db;

    /**
     * @var \Kernel\View\ViewContext
     */
    protected $view;


    public function __construct( Config\Config $config, $db=null, ViewContext $view)
    {
        $this->config = $db;
        $this->db = $db;
        $this->view = $view;
    }

    public function render_view($view, $parameters)
    {
        $this->view->render( @explode('\\',get_class($this))[1]."/view/".$view, $parameters);
    }
} 