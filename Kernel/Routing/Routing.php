<?php

namespace Kernel\Routing;

/**
 * Kernel routing class.
 * Created by Alexander Petrov.
 */
class Routing extends RoutingBase
{
    /**
     * @var $url - exploded REQUEST_URI;
     */
    protected $url;

    /**
     * @var $basicController - full path with namespace
     */
    protected $basicController;
    /**
     * @var $basicMethod
     */
    protected $basicMethod;

    public function __construct($config)
    {
        $this->config = $config;
        $this->basicMethod = $this->config['default_method'].$this->config['method_suffix'];
        $this->basicController = '\\'.$this->config['app_folder'].'\\'.$this->config['default_controller'].'\\'.$this->config['default_controller'].$this->config['controller_suffix'];
    }

    /**
     * Explode Url
     * @param $url
     * @return array;
     */
    public function explodeUrl($url)
    {
        $this->url = explode('/', trim($url, '/'));
        return $this->url;
    }

    /** get name of controller
     * @return string
     */
    public function getController()
    {
        if ($this->url[0]) {
            return '\\'.$this->config['app_folder'].'\\'.ucfirst(strtolower($this->url[0])).'\\'.ucfirst(strtolower($this->url[0])).$this->config['controller_suffix'];
        } else {
            return $this->basicController;
        }
    }

    /** get name of controller method
     * @return string
     */
    public function getMethod()
    {
        if (isset($this->url[1])) {
            return $this->url[1].$this->config['method_suffix'];
        } else {
            return $this->basicMethod;
        }
    }

    /**
     * get method parameters
     * @return array
     */
    public function getParams()
    {
        $params = array();
        if (sizeof($this->url) > 2) {
            for ($i=2; $i < sizeof($this->url); $i++) {
                $params[] = @$this->url[$i];
            }
        }
        return $params;
    }
} 