<?php

namespace Kernel\Routing;

/**
 * Base routing interface.
 * Created by Alexander Petrov.
 */
interface RoutingBaseInterface
{
    /**
     * Explode Url
     * @param $url
     * @return array;
     */
    public function explodeUrl($url);

    /** get name of controller
     * @return string
     */
    public function getController();

    /** get name of controller method
     * @return string
     */
    public function getMethod();

    /**
     * get method parameters
     * @return array
     */
    public function getParams();

}