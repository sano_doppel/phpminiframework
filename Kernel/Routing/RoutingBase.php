<?php

namespace Kernel\Routing;

/**
 * Base routing abstract class.
 * Created by Alexander Petrov.
 */
abstract class RoutingBase implements RoutingBaseInterface
{
    /**
     * @var $basicController
     */
    protected $basicController;
    /**
     * @var $basicMethod
     */
    protected $basicMethod;


    function __construct()
    {

    }
    function load($controller, $method, $params=array())
    {
        call_user_func_array(array($controller, $method), $params);
    }
} 