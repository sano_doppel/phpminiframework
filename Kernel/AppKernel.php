<?php
namespace Kernel;

use Kernel\Logger\Logger;
use Kernel\Logger\ErrorObservable;
use Kernel\Routing\Routing as Routing;
use Kernel\DB as KernelDB;
use Kernel\Config\Config;
use Kernel\View as KernelView;

/**
 * Kernel application class.
 * Created by Alexander Petrov.
 */
class AppKernel
{
    use Singleton;

    /**
     * @var $routing;
     */
    private $routing;

    /**
     * @var $config;
     */
    private $config;

    /**
     * @var $db;
     */
    private $db;

    /**
     * @var $view;
     */
    private $view;

    /**
     * @var $url
     */
    private $url;

    /**
     * init kernel
     * @param null $url
     */
    public function init($url = null)
    {

        $this->url = $url ? $url : $_SERVER['REQUEST_URI'];
        $this->loadConfig();
        $this->routing = new Routing(
            $this->config->load('config/kernel', 'kernel')
        );
    }

    /**
     *  load config
     */
    public function loadConfig()
    {
        $this->config = new Config();
        $this->config->setMethod('ini');
    }

    /**
     * set db
     */
    public function setDB()
    {
        $this->db = new KernelDB\DBContext(new KernelDB\MYSQLStrategy());
        $this->db->connect(
            $this->config->load('config/kernel', 'db')
        );
    }

    /**
     * init active records
     */
    public function setActiveRecord()
    {
        \ActiveRecord\Config::initialize(function($cfg)
        {
            $dbParams = $this->config->load('config/kernel', 'db');
            $cfg->set_model_directory(__DIR__.'/../models');
            $cfg->set_connections(array(
                'development' =>
                    sprintf(
                        '%s://%s:%s@%s/%s',
                        $dbParams['driver'],
                        $dbParams['user'],
                        $dbParams['password'],
                        $dbParams['host'],
                        $dbParams['db']
                    )
                )
            );
        });
    }

    /**
     * set Twig connection
     */
    public function setTwigConnection()
    {
        $this->view = new KernelView\ViewContext(new KernelView\TwigStrategy());
        $this->view->connect($this->config->load('config/kernel', 'view'));
    }

    /**
     * handle kernel
     */
    public function handle()
    {
        $this->_loadControllerMethod();
    }

    /**
     * load controller method by url
     */
    private function _loadControllerMethod()
    {
        $url = $this->routing->explodeUrl($this->url);
        $controller = $this->routing->getController();
        $method = $this->routing->getMethod();
        $params = $this->routing->getParams();
        $routingController = new $controller($this->config, $this->db, $this->view);
        $errorObservable = new ErrorObservable();
        $errorObservable->config($this->config->load('config/kernel', 'logger'));
        $logger = new Logger($errorObservable);

        try{
            $this->routing->load($routingController, $method, $params);
        } catch(\Exception $e) {
            $errorObservable->addError($e);
        }
    }
}


