<?php
namespace Kernel\DB;

/**
 * DB context class
 * Created by Alexander Petrov.
 */
class DBContext
{
    /**
     * @var $dbStrategy
     */
    private $dbStrategy;

    function __construct($strategy)
    {
        $this->dbStrategy = $strategy;
    }

    /**
     * Connect to DB
     * @param $params
     */
    function connect($params)
    {
        $this->dbStrategy->connect($params);
    }
    /** DB query
     * @param $query
     */
    function query($query)
    {
        $this->dbStrategy->query($query);
    }

    /** Get result
     * @param null $type
     * @return mixed
     */
    function fetch($type = null)
    {
        return $type ? $this->dbStrategy->fetch($type) : $this->dbStrategy->fetch();
    }
} 