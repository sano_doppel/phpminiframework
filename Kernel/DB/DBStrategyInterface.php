<?php

namespace Kernel\DB;

/**
 * DB strategy interface class
 * Created by Alexander Petrov.
 */
interface DBStrategyInterface
{
    /**
     * Connect to DB
     * @param $params
     */
    function connect($params);

    /** DB query
     * @param $query
     */
    function query($query);

    /**
     * Get result
     */
    function fetch();
}