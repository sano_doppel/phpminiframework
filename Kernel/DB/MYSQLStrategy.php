<?php

namespace Kernel\DB;

/**
 * MYSQL strategy class
 * Created by Alexander Petrov.
 */
class MYSQLStrategy implements DBStrategyInterface
{
    /**
     * @var $connection
     */
    private $connection;

    /**
     * @var $result
     */
    private $result;

    /**
     * Connect to DB
     * @param $params
     */
    public function connect($params)
    {
        $this->connection = mysqli_connect($params["host"], $params["user"], $params["password"], $params["db"]) or die("Error " . mysqli_error($this->connection));
    }

    /** DB query
     * @param $query
     */
    function query($query)
    {
        $this->result = $this->connection->query($query);
    }

    /**
     * Get result
     * @param string $type
     * @return array|null
     */
    function fetch($type = 'assoc')
    {
        $result = null;
        switch ($type) {
            case 'row':
                while ($row = $this->result->fetch_row()) {
                    $result[] = $row;
                }
                break;
            case 'object':
                while ($row = $this->result->fetch_object()) {
                    $result[] = $row;
                }
                break;
            case 'assoc':
                while ($row = $this->result->fetch_assoc()) {
                    $result[] = $row;
                }
                break;
        }
        return $result;
    }
}