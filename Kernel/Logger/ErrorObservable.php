<?php
namespace Kernel\Logger;

use Kernel\Observer\Observable;

/**
* Error Observable class
* Created by Alexander Petrov.
*/
class ErrorObservable  extends Observable
{

    /**
     * @var $errors[]
     */
    public $errors;

    /**
     * @var array $showErrors
     */
    protected $showErrors;

    /** config error object
     * @param $config
     */
    public function config($config)
    {
        $this->showErrors = @$config['show_errors'];
    }

    /** add errors
     * @param $error
     */
    public function addError($error)
    {
        $this->errors[] = $error;
        $this->notify();
        if ($this->showErrors) {
            echo $error->getMessage();
        }
    }

    /**
     * get las error
     * @return array
     */
    public function getLastError()
    {
        return $this->errors[ sizeof($this->errors) - 1 ];
    }
}