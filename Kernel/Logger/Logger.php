<?php
namespace Kernel\Logger;

use Kernel\Observer\Observable;
use Kernel\Observer\Observer;

/**
* Logger class
* Created by Alexander Petrov.
*/
class Logger extends Observer {

    /**
     * log errors
     * @param Observable $observable
     */
    function doUpdate(Observable $observable)
    {
        error_log($observable->getLastError()->getMessage());
    }
}