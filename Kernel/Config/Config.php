<?php

namespace Kernel\Config;

/**
 * Kernel config class
 * Created by Alexander Petrov.
 */
class Config implements ConfigInterface
{

    private $method;

    /**
     *  set config file method PHP|ini
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * get method
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }


    /**
     * load config array
     * @var $file string
     * @var $part string
     * @return array
     */
    public function load($file, $part = null)
    {
        switch ($this->method) {
            case 'php':
                $config = include_once(__DIR__.'/../../'.$file.'.php');
                if (!$part) {
                    return $config;
                } else {
                    return $config[$part];
                }


            default:

                if (!$part) {
                    return parse_ini_file(__DIR__.'/../../'.$file.'.ini', true);
                } else {
                    return parse_ini_file(__DIR__.'/../../'.$file.'.ini', true)[$part];
                }
        }
    }



} 