<?php

namespace Kernel\Config;

/**
 * Kernel config interface
 * Created by Alexander Petrov.
 */
interface ConfigInterface
{
    /**
     *  set config file method PHP|ini
     */
    public function setMethod($method);

    /**
     * get method
     * @return string
     */
    public function getMethod();

    /**
     * load config array
     * @var $file string
     * @var $part string
     * @return array
     */
    public function load($file, $part);
} 