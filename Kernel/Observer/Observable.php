<?php
namespace Kernel\Observer;

/**
 * Class Observable
 * @package Kernel\Observer
 */
class Observable implements \SplSubject
{

    protected $storage;
    function __construct()
    {
        $this->storage = new \SplObjectStorage();
    }

    function attach(\SplObserver $observer)
    {
        $this->storage->attach($observer);
    }

    function detach(\SplObserver $observer)
    {
        $this->storage->detach($observer);
    }

    function notify()
    {
        foreach($this->storage as $obj) {
            $obj->update($this);
        }
    }

} 