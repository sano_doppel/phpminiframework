<?php
namespace Kernel\View;

/**
 * View context class
 * Created by Alexander Petrov.
 */
class ViewContext
{

    /**
     * @var $viewStrategy
     */
    private $viewStrategy;

    function __construct($strategy)
    {
        $this->viewStrategy = $strategy;
    }

    /**
     * View connection
     * @param $params
     */
    function connect($params)
    {
        $this->viewStrategy->connect($params);
    }


    /** View rendering
     * @param $view
     * @param $params
     */
    function render($view, $params)
    {
        $this->viewStrategy->render($view, $params);
    }

} 