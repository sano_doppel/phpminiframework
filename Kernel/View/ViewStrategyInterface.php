<?php

namespace Kernel\View;

/**
 * View strategy interface class
 * Created by Alexander Petrov.
 */
interface ViewStrategyInterface
{
    /**
     * View connection
     * @param $params
     */
    function connect($params);

    /** View rendering
     * @param $view
     * @param $params
     */
    function render($view, $params);

}