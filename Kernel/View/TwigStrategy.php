<?php
namespace Kernel\View;

/**
 * TwigStrategy class
 * Created by Alexander Petrov.
 */
class TwigStrategy implements ViewStrategyInterface
{

    /**
     * @var $twig
     */
    private $twig;

    /**
     *
     * View connection
     * @param $params
     */
    function connect($params)
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/../../'.$params['app_folder']);
        $this->twig = new \Twig_Environment($loader, array(
            'debug' => true
        ));
        $this->twig->addExtension(new \Twig_Extension_Debug());
    }


    /** View rendering
     * @param $view
     * @param $params
     */
    function render($view, $params)
    {
        echo $this->twig->render($view, $params);
    }
} 