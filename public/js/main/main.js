function showScore() {
    var additional = $('.additional');
    if ( additional.hasClass('additional__modal') ) {
        additional.removeClass('additional__modal');
    } else {
        var margin = 20;
        var scoreButton = $('.main__show_score');
        additional.css({ top: scoreButton.offset().top , left: scoreButton.offset().left - additional.width() - margin})
        additional.addClass('additional__modal');
    }

}

function setName() {
    var name = $('#session_name').val();
    $.ajax({
        url: "main/setName",
        data: { name : name },
        method: "POST",
        dataType: "json",
        success: function() {
            $('.modal').remove();
            $('#header .username').text(name);
        }
    });
}
function startGame() {
    $('.input-block input, .input-block button').attr('disabled','disabled');
    $('.input-block__item').removeClass('input-block__item').addClass('input-block__item__disable');
    $('.input-block__start').removeClass('input-block__start').addClass('input-block__start__disable');
    pong.init();
    var pongPlay = setInterval(function() {
        pong.play();
        if ( !pong.isStarted() ) {

            $('.input-block input, .input-block button').attr('disabled',false);
            $('.input-block__item__disable').removeClass('input-block__item__disable').addClass('input-block__item');
            $('.input-block__start__disable').removeClass('input-block__start__disable').addClass('input-block__start');
            clearInterval(pongPlay);
            endGame(pong.getScore());
        }
    }, 30);
}

function endGame(score){
    $.ajax({
        url: "main/endGame",
        data: { score : score },
        method: "POST",
        dataType: "json",
        success: function(data) {
            if (!data.error)
            {
                changeScore();
            }
        }
    });
}

function changeScore() {
    $.ajax({
        url: "main/getScore",
        method: "POST",
        dataType: "html",
        success: function (data) {
            $('.additional').html(data);
        }
    });
}

function Canvas() {

    var config = {
        canvasSize: {x: 600, y: 500},
        ballPosition: {x: 300, y: 250},
        padPosition: {
            player1: {x: 0, y: 220, width: 10, height: 90},
            player2: {x: 590, y: 220, width: 10, height: 90}
        },
        ballRadius: 10
    };


    var canvas = $('#pongCanvas');
    var context  = canvas[0].getContext("2d");
    var ballPosition = $.extend(true, {}, config.ballPosition );
    var padPosition = $.extend(true, {}, config.padPosition );
    var livesNumber = 0;
    var scoreNumber = 0;
    this.getConfig = function() {
        return config;
    };

    this.restore = function() {
        ballPosition = $.extend(true, {}, config.ballPosition );
        padPosition = $.extend(true, {}, config.padPosition );
    };

    this.update = function(ball, padsDirection, lives, score) {
        var radians = ball.angle * Math.PI / 180;
        livesNumber = lives;
        scoreNumber = score;
        ballPosition.x += Math.cos(radians) * ball.speed;
        ballPosition.y += Math.sin(radians) * ball.speed;

        for (var key in padsDirection)
        {
            var pad = null;

            switch (key) {
                case 'player1':
                    pad = padPosition.player1;
                    break;
                case 'player2':
                    pad = padPosition.player2;
                    break;
            }

            switch (padsDirection[key]){
                case 'UP':
                    pad.y = pad.y - parseInt(pad.height/4);
                    if ( pad.y < 0 ) {
                        pad.y = 0;
                    }
                    break;
                case 'DOWN':
                    pad.y = pad.y + parseInt(pad.height/4);
                    if ( pad.y > config.canvasSize.y - pad.height ) {
                        pad.y = config.canvasSize.y -  pad.height;
                    }
                    break;
            }
        }


        return { ballPosition: {x: ballPosition.x, y: ballPosition.y}, padPosition: padPosition}
    };

    this.draw = function() {
        var drawField = function() {
            context.fillStyle = "#151421";
            context.fillRect(0, 0, config.canvasSize.x, config.canvasSize.y);

            context.beginPath();
            context.strokeStyle = '#ffffff';
            context.moveTo(config.canvasSize.x/2, 0);
            context.lineTo(config.canvasSize.x/2, config.canvasSize.y);
            context.closePath();
            context.stroke();
        };

        var drawBall = function(ballPosition) {

            context.arc(ballPosition.x, ballPosition.y, config.ballRadius, 0, 2 * Math.PI, false);
            context.fillStyle = '#ffffff';
            context.fill();
        };

        var drawPad = function(padPosition) {

            context.fillStyle = "#ffffff";
            context.fillRect(padPosition.x, padPosition.y, padPosition.width, padPosition.height);


        };

        var drawText = function(lives, score) {
            context.font = "20px Arial";
            context.fillText("Lives: "+ lives, 500, 20);
            context.fillText("Score: "+ score, 10, 20);
        };

        drawField();
        drawBall(ballPosition);
        drawPad(padPosition.player1);
        drawPad(padPosition.player2);
        drawText(livesNumber, scoreNumber);
    };

}

function Pong(canvas) {

    var lives = 0;
    var difficult = 1;
    var config = {};
    var isStarted = false;
    var ballAngle;
    var score;
    var defaultLives;
    var changeDirectionFreeze;
    var padsDirection = {player1: null, player2: null};

    this.getScore = function() {
        return score;
    };

    this.startAngle = function(player) {
        switch ( player ) {
            case 1:
                if ( Math.round(Math.random()) ) {
                    return 200 + parseInt(Math.random() * 30);
                } else {
                    return 160 - parseInt(Math.random() * 30);
                }
            case 2:
            {
                if ( Math.round(Math.random()) ) {
                    return 20 + parseInt(Math.random() * 30);
                } else {
                    return 340 - parseInt(Math.random() * 30);
                }
            }
        }
    };

    this.init = function() {
        defaultLives = parseInt($('#lives').val());
        lives = defaultLives;
        difficult = parseInt($('#difficult').val());
        config = canvas.getConfig();
        ballAngle = this.startAngle(1);
        score = 0;
    };

    this.isStarted = function() {
        return isStarted;
    };

    this.padMove = function(player, direction) {

        var padDirection = null;
        switch ( direction ) {
            case 'UP':
                padDirection = 'UP';
                break;
            case 'DOWN':
                padDirection = 'DOWN';
                break;
        }

        switch ( player ) {
            case 1:
                padsDirection.player1 = padDirection;
                break;
            case 2:
                padsDirection.player2 = padDirection;
                break;
        }

    };

    this.collide = function (data) {
        if ( changeDirectionFreeze ) {
            changeDirectionFreeze -= 1;
        }

        var ballPosition = data.ballPosition;
        var pad = data.padPosition;
        if ( ballPosition.y - config.ballRadius <= 0 || ballPosition.y + config.ballRadius >= config.canvasSize.y) {
            return 'VERTICAL_BORDER';
        } else if ( ballPosition.x - config.ballRadius <= 0 ) {
            return 'PLAYER_LOSE';
        } else if ( ballPosition.x + config.ballRadius >= config.canvasSize.x ) {
            return 'PLAYER_WIN';
        } else if ( ballPosition.x - config.ballRadius <= pad.player1.x+pad.player1.width &&
            ballPosition.y + config.ballRadius >= pad.player1.y &&
            ballPosition.y - config.ballRadius <= pad.player1.y+pad.player1.height
        ) {
            if (changeDirectionFreeze)
            {
                return;
            }
            changeDirectionFreeze = 5;
            return 'PAD_1';
        } else if ( ballPosition.x + config.ballRadius >= pad.player2.x &&
            ballPosition.y + config.ballRadius >= pad.player2.y &&
            ballPosition.y - config.ballRadius <= pad.player2.y+pad.player2.height
        ) {
            if ( changeDirectionFreeze ) {
                return;
            }
            changeDirectionFreeze = 5;
            return 'PAD_2';
        }
        return false;
    };

    this.computerMove = function(data, difficult, ballAngle) {
        var random = Math.floor(Math.random() * 12);
        if ( 2 * difficult < random || ( ballAngle > 90 && ballAngle < 270) ) {
            return null;
        }
        if ( data.ballPosition.y > data.padPosition.player2.y + data.padPosition.player2.height ) {
            return 'DOWN'
        } else if ( data.ballPosition.y < data.padPosition.player2.y ) {
            return 'UP'
        } else {
            return null;
        }
    };

    this.play = function() {

        if ( !this.validate() ) {
            return;
        }

        isStarted = 1;

        if (!lives) {
            isStarted = 0;
            canvas.update({angle: 0, speed: 0}, padsDirection, lives, score);
            canvas.draw();
            return;
        }


        var updateResult = canvas.update({angle: ballAngle, speed: (3*difficult + Math.ceil(3/difficult)) }, padsDirection, lives, score);
        padsDirection.player2 = this.computerMove(updateResult, difficult, ballAngle);

        switch ( this.collide(updateResult) ) {
            case 'VERTICAL_BORDER':
                ballAngle = 360 - ballAngle;
                break;
            case 'PLAYER_LOSE':
                ballAngle = this.startAngle(2);
                lives = lives - 1;
                canvas.restore();
                break;
            case 'PLAYER_WIN':
                ballAngle = this.startAngle(1);
                score += (100/defaultLives) * difficult;
                canvas.restore();
                break;
            case 'PAD_1':
                ballAngle = 180 - ballAngle;
                score += (10/defaultLives) * difficult;
                break;
            case 'PAD_2':
                ballAngle = 180 - ballAngle;
                break;
        }

        if ( ballAngle <= -360 ) {
            ballAngle += 360;
        } else if ( ballAngle >= 360 ) {
            ballAngle  -= 360;
        }
        canvas.draw();
    };


    this.validate = function() {

        if ( lives < 0 || lives > 10 ) {
            return 0;
        }

        if ( difficult < 1 || difficult > 3 ) {
            return 0;
        }

        return 1;
    };
}
var canvas = new Canvas();
var pong = new Pong(canvas);

canvas.draw();


$('.input-block__start').click(function() {
    if ( pong.isStarted() ) {
        return;
    }

    $.ajax({
        url: "main/startGame",
        data: { difficult : parseInt($('#difficult').val()) },
        method: "POST",
        dataType: "json",
        success: function(data) {
            if (!data.error)
            {
                startGame();
            }

        }
    });
});

$('#lives').on('change', function() {
    if ( parseInt($(this).val()) > 10 )
    {
        $(this).val(10);
    }
});

$(document).keydown(function(e) {
    if (pong.isStarted()) {
        switch(e.which) {

            case 38:
                pong.padMove(1, 'UP');
                break;

            case 40:
                pong.padMove(1, 'DOWN');
                break;

        }
        e.preventDefault();
    }
});

$(document).keyup(function(e) {
    if (pong.isStarted()) {
        switch(e.which) {

            case 38:
                pong.padMove(1, null);
                break;

            case 40:
                pong.padMove(1, null);
                break;

        }
        e.preventDefault();
    }
});