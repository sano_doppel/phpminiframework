<?php

class MainTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \App\Main\Helper\MainHelper
     */
    private $helper;

    /***
     * @var Session
     */
    private $session;

    public function setUp()
    {
        $this->helper = new \App\Main\Helper\MainHelper();
        $this->session = null;
    }

    public function tearDown()
    {

        if (!$this->session) {
            return;
        }
        $games = $this->session->games;
        foreach ($games as $game) {
            $game->delete();
        }
        $this->session->delete();

    }

    public function testCreateSession()
    {
        $this->assertEquals(false, $this->helper->getHash());
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $_SERVER['HTTP_USER_AGENT'] = 'test';
        $_COOKIE['hash'] = $this->helper->createSession();
        $session = $this->helper->getSession();
        $this->assertEquals($_COOKIE['hash'], $this->helper->getHash());
        $this->assertEquals($_COOKIE['hash'], $session->hash);
        return $session;
    }

    public function testGetScore()
    {
        $score = $this->helper->getScore();
        $scoreTest = \Game::all(
            array(
                'conditions'=>array('ended_at is NOT NULL AND score is NOT NULL'),
                'order' =>'score DESC'
            )
        );

        $diff = false;
        foreach ($scoreTest as $key => $value) {
            if ($scoreTest[$key]->score != $score[$key]->score){
                $diff = true;
                break;
            }
        }
        $this->assertFalse($diff);
    }

    /**
     * @param $session
     * @depends testCreateSession
     */
    public function testSetName($session)
    {
        $name = 'tester';
        $_COOKIE['hash'] = $session->hash;
        $this->helper->setName($name);
        $this->assertEquals($name, $this->helper->getSession()->name);
    }

    /**
     * @param $session
     * @depends testCreateSession
     */
    public function testGame($session)
    {
        $this->session = $session;
        $_COOKIE['hash'] = $session->hash;
        $difficult = 1;
        $this->helper->createGame($difficult);
        $games = $this->helper->getSession()->games;
        $this->assertNotEmpty($games);
        if (isset($games[0])) {
            $this->assertEquals($difficult, $games[0]->difficult);
        }
        $score = 10;
        $this->helper->endGame($score);
        $games = $this->helper->getSession()->games;
        if (isset($games[0])) {
            $this->assertEquals($score, $games[0]->score);
        }
    }
}
