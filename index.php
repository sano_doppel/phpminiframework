<?php

use Kernel\AppKernel;

spl_autoload_register(function ($class) {
    $filename = __DIR__.'/' . str_replace('\\', '/', $class) . '.php';
    include_once($filename);
});

$cli = false;
$url = null;
if (php_sapi_name() == 'cli' or PHP_SAPI == 'cli') {
    $cli = true;
    $url = @$argv[1];
}

require_once 'vendor/autoload.php';

$kernel = AppKernel::getInstance();
$kernel->init($url);

$kernel->setTwigConnection();

/* simple DB class
$kernel->setDB();
*/

/* Active Record */
$kernel->setActiveRecord();

$kernel->handle();
die;
