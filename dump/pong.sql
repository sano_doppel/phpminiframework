--
-- Database: `pong`
--
CREATE DATABASE IF NOT EXISTS `pong` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pong`;

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `difficult` enum('1','2','3') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `score` int(11) DEFAULT NULL,
  `started_at` datetime NOT NULL,
  `ended_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `session_id`, `difficult`, `score`, `started_at`, `ended_at`) VALUES
(1, 1, '3', 12, '2015-11-03 14:29:01', '2015-11-03 14:29:23'),
(2, 1, '3', 60, '2015-11-03 14:29:30', '2015-11-03 14:29:38');

--
-- Triggers `games`
--
DROP TRIGGER IF EXISTS `games_insert`;
DELIMITER //
CREATE TRIGGER `games_insert` BEFORE INSERT ON `games`
 FOR EACH ROW SET NEW.started_at = NOW()
//
DELIMITER ;
DROP TRIGGER IF EXISTS `games_update`;
DELIMITER //
CREATE TRIGGER `games_update` BEFORE UPDATE ON `games`
 FOR EACH ROW IF NEW.score IS NOT NULL THEN
	SET NEW.ended_at = NOW();
END IF
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `hash` varchar(100) NOT NULL,
  `user_agent` text NOT NULL,
  `ip` varchar(20) NOT NULL,
  `last_update` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `name`, `hash`, `user_agent`, `ip`, `last_update`, `is_active`) VALUES
(1, 'Alexander', '5H9Djly8qNxbVXynWBtVIEOERwm8uITAq3dJpMRQzp2umASjbl78802371692b9579823ac913717ce986', 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', '127.0.0.1', '2015-11-03 14:28:54', 1);

--
-- Triggers `sessions`
--
DROP TRIGGER IF EXISTS `sessions_insert`;
DELIMITER //
CREATE TRIGGER `sessions_insert` BEFORE INSERT ON `sessions`
 FOR EACH ROW SET NEW.last_update = NOW()
//
DELIMITER ;
