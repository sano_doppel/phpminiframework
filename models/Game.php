<?php

class Game extends ActiveRecord\Model
{
    static $table_name = 'games';
    static $belongs_to = array(
        array('session')
    );

    /**
     * @return Session
     */
    function getSession()
    {
        return $this->session;
    }
}