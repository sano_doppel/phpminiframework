<?php

class Session extends ActiveRecord\Model
{
    static $table_name = 'sessions';
    static $has_many = array(
        array('games', 'order' => 'started_at desc')
    );

    /**
     * @return Game
     */
    public function getGame()
    {
        return Game::find(
            array(
                'conditions' => sprintf('session_id = %d AND ended_at is NULL AND started_at is NOT NULL', $this->id),
                'order' => 'started_at DESC'
            )
        );
    }
}