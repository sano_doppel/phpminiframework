<?php

use Kernel\AppKernel;

spl_autoload_register(function ($class) {
    if ( in_array(explode('\\', $class)[0], array('App', 'Kernel')) )
    {
        $filename = __DIR__.'/' . str_replace('\\', '/', $class) . '.php';
        include_once($filename);
    }

});

require_once 'vendor/autoload.php';

\ActiveRecord\Config::initialize(function($cfg)
{
    $config = new \Kernel\Config\Config();
    $dbParams = $config->load('config/kernel', 'db');
    $cfg->set_model_directory(__DIR__.'/models');
    $cfg->set_connections(array(
            'development' =>
                sprintf(
                    '%s://%s:%s@%s/%s',
                    $dbParams['driver'],
                    $dbParams['user'],
                    $dbParams['password'],
                    $dbParams['host'],
                    $dbParams['db']
                )
        )
    );
});
