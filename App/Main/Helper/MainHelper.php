<?php
namespace App\Main\Helper;

use ActiveRecord\DateTime;

class MainHelper
{
    /**
     * @param $length
     * @return string
     */
    public function randomString($length)
    {
        $string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $stringLength = strlen($string);
        $random = '';
        for ($i = 0; $i < $length; $i++) {
            $random .= $string[rand(0, $stringLength - 1)];
        }
        return $random;
    }

    /**
     * get session
     * @return object
     */
    public function getSession()
    {
        if (!isset($_COOKIE['hash'])) {
            return false;
        }
        return \Session::find_by_hash($_COOKIE['hash']);
    }

    /**
     * get hash
     * @return bool
     */
    public function getHash()
    {
        $session = $this->getSession();
        return $session ? $session->hash : false ;
    }

    /**
     * create session
     * @return bool|string
     */
    public function createSession()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $hash = $this->randomString(50) . md5(time());
        if ($_SERVER['HTTP_USER_AGENT'] != 'test') {
            setcookie('hash', $hash, time()+24*3600);
        }

        $session = new \Session(
            array(
                'hash' => $hash,
                'ip' => $ip,
                'user_agent'=>$_SERVER['HTTP_USER_AGENT'],
                'is_active' => 1)
        );

        return $session->save() ? $hash : false;
    }

    /**
     * create new game
     * @param $difficult
     * @return bool
     */
    public function createGame($difficult)
    {
        $session = $this->getSession();
        $session->create_game(array('difficult' => (int) $difficult));
        return true;
    }

    /**
     * @param $score
     * @return bool
     */
    public function endGame($score)
    {

        $session = $this->getSession();
        $game = $session->getGame();
        if (!$game) {
            return false;
        }
        $game->score = $score;
        $game->save();
        return true;
    }

    /**
     * get score
     * @return array
     */
    public function getScore()
    {
        return \Game::all(
            array(
                'conditions'=>array('ended_at is NOT NULL AND score is NOT NULL'),
                'order' =>'score DESC'
            )
        );
    }

    /**
     * remove old unfinished games
     */
    public function removeUnfinishedGames() {
        $startedDate = new DateTime();
        $startedDate->modify('-7 day');

        $games = \Game::all(
            array(
                'conditions'=>array(
                    sprintf('ended_at is NULL AND started_at < "%s"', $startedDate->format('Y-m-d H:i:s'))
                )
            )
        );
        foreach ($games as $game) {
            $game->delete();
        }
    }

    /**
     * Set username
     * @param $name
     * @return bool
     */
    public function setName($name)
    {
        $session = $this->getSession();
        $session->name = $name;
        return $session->save() ? true : false;
    }

}