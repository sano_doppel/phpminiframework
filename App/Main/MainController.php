<?php
namespace App\Main;

use Kernel\BaseController;
use User;

class MainController extends BaseController
{

    public function indexAction()
    {
        $helper = new Helper\MainHelper();
        $hash = $helper->getHash();

        if (!$hash) {
            $helper->createSession();
        }

        $this->view->render(
            'main/index.twig', array(
                'score' => $helper->getScore(),
                'session' => $helper->getSession()
            )
        );
    }

    public function startGameAction()
    {
        $helper = new Helper\MainHelper();
        if (!isset($_POST['difficult'])) {
            echo json_encode(array('error' => 1));
            return false;
        }
        if ($helper->createGame($_POST['difficult'])) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function endGameAction()
    {
        $helper = new Helper\MainHelper();
        if (!isset($_POST['score'])) {
            echo json_encode(array('error' => 1));
            die();
        }

        if ($helper->endGame($_POST['score'])) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function getScoreAction()
    {
        $helper = new Helper\MainHelper();
        $this->view->render(
            'main/score.twig', array(
                'score' => $helper->getScore()
            )
        );
    }

    public function setNameAction()
    {
        if (!isset($_POST['name'])) {
            echo json_encode(array('error' => 1));
            die();
        }
        $helper = new Helper\MainHelper();
        $helper->setName($_POST['name']);
        echo 1;
    }

    public function cronRemoveUnfinishedAction()
    {
        $helper = new Helper\MainHelper();
        $helper->removeUnfinishedGames();
    }
} 